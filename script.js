//função que vai fazer o botao pegar as letras e as palavras e retornar a quantidade de vezes que elas aparecem
document.getElementById("countButton").onclick = function clique() {

    const lettersDiv = document.getElementById("lettersDiv");
    lettersDiv.innerText = " ";
    const wordsDiv = document.getElementById("wordsDiv");
    wordsDiv.innerText = " ";
        //para registrar o número de vezes que cada letra é usada no texto.
    const letterCounts = {

    };

    //texto digitado
    let typedText = document.getElementById("textInput").value;
    // Isto muda todas as letras para minúsculas
    typedText = typedText.toLowerCase();

    // Isso se livra de todos os caracteres exceto letras comuns, espaços e apóstrofos. 
    typedText = typedText.replace(/[^a-z'\s]+/g, "");

    //faz um loop em todas letras da caixa de texto
    for (let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];
        // faça algo com cada letra 
        //muda o valor de contagem a cada vez que a letra aparece
        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
        } else {
            letterCounts[currentLetter]++;
        }


    }

    //Para saber quantas vezes cada letra apareceu
    for (let letter in letterCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        span.appendChild(textContent);
        lettersDiv.appendChild(span);
    }


    const palavras = {

    };
    //modo de separar a string de entrada em palavras separando-a nos espaços.
    words = typedText.split(/\s/);
    console.log(words);

    for (let i = 0; i < words.length; i++) {
        currentWord = words[i];

        if (palavras[currentWord] === undefined) {
            palavras[currentWord] = 1;
        } else {
            palavras[currentWord]++;
        }
    }

    for (let word in palavras) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + word + "\": " + palavras[word] + ", ");
        span.appendChild(textContent);
        wordsDiv.appendChild(span);
    }



}